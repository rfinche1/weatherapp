package com.rasfincher.weatherapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity {
    private double latitude = 33.575;
    private double longitude = -85.098;
    private ExpandableListAdapter adapter;
    private ExpandableListView expandableListView;
    private FetchThread fetcher;
    private Handler handler;
    private List<String> expandableListTitles;
    private HashMap<String, List<String>> expandableListDetailItems;
    private Runnable prepareAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        } else {
            startApp();
        }
    }

    public void startApp() {
        this.setLocation();
        this.prepareExpandableList();

        this.fetcher = new FetchThread(this.getForecastURL());
        this.fetcher.start();

        this.handler = new Handler();
        this.handler.post(this.checkFetcher);

        this.prepareAdapter = new Runnable() {
            public void run() {
                adapter = new CustomExpandableListAdapter(MainActivity.this, expandableListTitles, expandableListDetailItems);
                expandableListView.setAdapter(adapter);
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0 && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                startApp();
                return;
            } else {
                System.exit(-1);
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void prepareExpandableList() {
        this.expandableListView = findViewById(R.id.expandableListView);
        this.expandableListTitles = new ArrayList<>();
        this.expandableListDetailItems = new HashMap<>();
    }

    private void setLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            this.checkForPermissions(Manifest.permission.ACCESS_FINE_LOCATION);
            this.checkForPermissions(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(location != null){
            this.setLatitude(location.getLatitude());
            this.setLongitude(location.getLongitude());
        } else {
            this.setLatitude(latitude);
            this.setLongitude(longitude);
        }
    }

    Runnable checkFetcher = new Runnable(){
        int count = 0;
        public void run(){
            if(fetcher.isFinished()){
                if(fetcher.successful()) {
                    prepareForecastData();
                    MainActivity.this.runOnUiThread(prepareAdapter);
                }
                else{
                    Toast.makeText(MainActivity.this, "Could not load weather data. Please try again later",
                            Toast.LENGTH_LONG).show();
                }
            }
            else{
                count++;
                int TIMEOUT = 240;
                if(count < TIMEOUT) {
                    handler.postDelayed(checkFetcher, 1000);
                }
            }
        }
    };

    private void prepareForecastData() {
        ForecastParser parser = new ForecastParser(this.fetcher.getResult());
        this.expandableListTitles = parser.getExpandableListTitles();
        this.expandableListDetailItems = parser.getExpandableListDetailItems();
    }

    private void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    private double getLatitude() {
        return this.latitude;
    }

    private void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    private double getLongitude() {
        return this.longitude;
    }

    private String getForecastURL() {
        return "https://api.weather.gov/points/" + this.getLatitude() + "," + this.getLongitude() + "/forecast";
    }

    private void checkForPermissions(String resource) {
        int result = ContextCompat.checkSelfPermission(this, resource);

        if(result == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this, new String[]{resource},0);
        }
    }

    public void refresh(View view) {
        this.fetcher = new FetchThread(this.getForecastURL());
        this.fetcher.start();

        this.handler = new Handler();
        this.handler.post(this.checkFetcher);
    }
}
