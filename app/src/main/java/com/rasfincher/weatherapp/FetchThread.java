package com.rasfincher.weatherapp;

import android.util.Log;

import java.net.URL;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Ras Fincher on 3/13/2018.
 */

public class FetchThread extends Thread {
    final String userAgent = "Android weather app";
    final String acceptString = "text/html,application/*,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
    final String encodingString = "gzip";
    final String languageString = "en-US,en;q=0.5";
    final String request;

    private StringBuffer sb;
    private boolean finished;
    private boolean success;

    public FetchThread(String request){
        this.request = request;
        sb = new StringBuffer();
        finished = false;
        success = false;
    }

    public boolean isFinished(){
        return finished;
    }

    public boolean successful() { return success; }
    @Override
    public void run() {
        URL url = null;
        HttpsURLConnection connection = null;
        Scanner scan = null;
        try{
            Log.v("fetch"," fetching");
            url = new URL(request);
            Log.v("fetch","opening connection");
            connection = (HttpsURLConnection) url.openConnection();

            if(connection == null){
                Log.v("fetch", "null connection");
                return;
            }
            connection.setRequestProperty("User-Agent",userAgent);
            connection.setRequestProperty("Accept",acceptString);
            connection.setRequestProperty("Accept-Encoding",acceptString);
            connection.setRequestProperty("Accept-Language",languageString);

            Log.v("fetch","getting response code");
            int response = connection.getResponseCode();
            Log.v("fetch","response = "+response);
            Log.v("fetch","attaching scanner");
            if(response == HttpsURLConnection.HTTP_OK) {

                scan = new Scanner(connection.getInputStream());
            }else {
                scan = new Scanner(connection.getErrorStream());
                sb.append("connection not ok\n");
            }
            while(scan.hasNext()){
                String line = scan.nextLine();
                sb.append(line);
            }

            Log.v("fetch","past scanning");
            scan.close();
            success = true;
        }catch(Exception err){
            Log.v("fetch","exception caught: "+err.getMessage());
            err.printStackTrace();
        }
        finished = true;
        return;
    }
    public String getResult(){
        return sb.toString();
    }
}
