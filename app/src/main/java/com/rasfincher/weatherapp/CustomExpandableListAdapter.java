package com.rasfincher.weatherapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Ras Fincher on 3/10/2018.
 */

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<String> expandableListTitle;
    private HashMap<String, List<String>> expandableListDetail;

    public CustomExpandableListAdapter(Context context, List<String> expandableListTitle,
                                       HashMap<String, List<String>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = (String) getChild(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.forecast_detail_item, null);
        }
        TextView expandedListTextView = (TextView) convertView
                .findViewById(R.id.forecastDay);
        expandedListTextView.setText(expandedListText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.forecast_day_group, null);
        }
        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.groupHeading);
        LinearLayout headerBackground = (LinearLayout) convertView.findViewById(R.id.headerBackground);
        ImageView weatherIcon = (ImageView) convertView.findViewById(R.id.forecastImg);

        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);

        if(listTitle.toLowerCase().contains("night")) {
            if(listTitle.toLowerCase().contains("rain")) {
                weatherIcon.setImageResource(R.drawable.rain);
                headerBackground.setBackgroundResource(R.drawable.rain_night_background);
                listTitleTextView.setTextColor(Color.WHITE);

            } else if(listTitle.toLowerCase().contains("thunderstorms")) {
                weatherIcon.setImageResource(R.drawable.thunderstorm);
                headerBackground.setBackgroundResource(R.drawable.thunderstorm_night_background);
                listTitleTextView.setTextColor(Color.BLACK);

            } else if(listTitle.toLowerCase().contains("wind")) {
                weatherIcon.setImageResource(R.drawable.windy);
                headerBackground.setBackgroundResource(R.drawable.windy_night_background);
                listTitleTextView.setTextColor(Color.WHITE);

            } else if(listTitle.toLowerCase().contains("snow")) {
                weatherIcon.setImageResource(R.drawable.snow);
                headerBackground.setBackgroundResource(R.drawable.clear_night_background);
                listTitleTextView.setTextColor(Color.BLACK);

            } else {
                listTitleTextView.setTextColor(Color.BLACK);
                weatherIcon.setImageResource(R.drawable.moon);
                headerBackground.setBackgroundResource(R.drawable.clear_night_background);
            }

        } else {
            listTitleTextView.setTextColor(Color.BLACK);
            if(listTitle.toLowerCase().contains("rain")) {
                weatherIcon.setImageResource(R.drawable.rain);
                headerBackground.setBackgroundResource(R.drawable.rain_day_background);

            } else if(listTitle.toLowerCase().contains("thunderstorms")) {
                weatherIcon.setImageResource(R.drawable.thunderstorm);
                headerBackground.setBackgroundResource(R.drawable.thunderstorm_day_background);

            } else if(listTitle.toLowerCase().contains("wind")) {
                weatherIcon.setImageResource(R.drawable.windy);
                headerBackground.setBackgroundResource(R.drawable.windy_day_background);

            } else if(listTitle.toLowerCase().contains("snow")) {
                weatherIcon.setImageResource(R.drawable.snow);
                headerBackground.setBackgroundResource(R.drawable.snow_day_background);

            } else {
                weatherIcon.setImageResource(R.drawable.sunny);
                headerBackground.setBackgroundResource(R.drawable.sunny_day_background);
            }
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}
