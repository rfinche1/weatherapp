package com.rasfincher.weatherapp;

import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ras Fincher on 3/10/2018.
 */

public class ForecastParser {
    private JSONObject forecast;
    private static final char DEGREES = '\u00B0';

    public ForecastParser(String jsonFile){
        try {
            forecast = new JSONObject(jsonFile);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public List<String> getExpandableListTitles() {
        JSONObject properties;
        JSONArray periods;
        List<String> expandableListTitles = new ArrayList<>();

        try {
            properties = this.forecast.getJSONObject("properties");
        } catch (JSONException e) {
            return expandableListTitles;
        }

        try {
            periods = properties.getJSONArray("periods");
        } catch (JSONException e) {
            return expandableListTitles;
        }

        for (int i = 0; i < periods.length(); i++) {
            try {
                expandableListTitles.add(this.buildTitleString(periods, i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return expandableListTitles;
    }

    @NonNull
    private String buildTitleString(JSONArray periods, int i) throws JSONException {
        return periods.getJSONObject(i).getString("name") + " - "
                + periods.getJSONObject(i).getString("shortForecast")
                + " - " + periods.getJSONObject(i).getString("temperature") + DEGREES + " F";
    }

    public HashMap<String, List<String>> getExpandableListDetailItems() {
        JSONObject properties;
        JSONArray periods;
        HashMap<String, List<String>> expandableListDetailItems = new HashMap<>();

        try {
            properties = this.forecast.getJSONObject("properties");
        } catch (JSONException e) {
            return expandableListDetailItems;
        }

        try {
            periods = properties.getJSONArray("periods");
        } catch (JSONException e) {
            return expandableListDetailItems;
        }

        for (int i = 0; i < periods.length(); i++) {
            String title = "";
            String temp = "";
            try {
                temp = periods.getJSONObject(i).getString("temperature");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                title = this.buildTitleString(periods, i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            List<String> details = new ArrayList<String>();
            try {
                details.add(periods.getJSONObject(i).getString("detailedForecast"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            expandableListDetailItems.put(title, details);
        }


        return expandableListDetailItems;
    }

}
